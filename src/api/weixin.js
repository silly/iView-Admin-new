import axios from '@/libs/api.request'
const baseUrl = '/v1/weixin'

/**
 * 查询公众号列表
 * @param {*} param0 查询参数
 */
export const getWeixinList = ({
  pageSize,
  pageNo,
  Type,
  Name
}) => {
  const data = {
    PageSize: pageSize || 10,
    PageNo: pageNo || 0,
    Type,
    Name
  }
  return new Promise((resolve, reject) => {
    axios.request({
      url: baseUrl + '/',
      params: data,
      method: 'get'
    }).then((res) => {
      resolve(res)
    }, (err) => {
      reject(err)
    })
  })
}

/**
 * 查询公众号类型
 */
export const getAccountTypes = () => {
  return new Promise((resolve, reject) => {
    axios.request({
      url: baseUrl + '/accountType',
      method: 'get'
    }).then((res) => {
      resolve(res)
    }, (err) => {
      reject(err)
    })
  })
}

// 公众号任务相关

/**
 * 添加公众号采集任务
 */
export const addToTaskList = (name, url) => {
  const data = {
    GongzhonghaoName: name,
    Url: url
  }
  return new Promise((resolve, reject) => {
    axios.request({
      url: baseUrl + '/insertTask',
      data,
      method: 'post'
    }).then((res) => {
      resolve(res)
    }, (err) => {
      reject(err)
    })
  })
}

/**
 * 查询公众号任务
 */
export const getTaskList = ({
  status,
  type,
  key,
  name,
  pageSize,
  pageNo
}) => {
  return new Promise((resolve, reject) => {
    const data = {
      Status: status || -1,
      Key: key || '',
      Name: name || '',
      Type: type || '',
      PageSize: pageSize || 10,
      PageNo: pageNo || 0
    }
    axios.request({
      url: baseUrl + '/tasks',
      method: 'get',
      params: data
    }).then((res) => {
      resolve(res)
    }, (err) => {
      reject(err)
    })
  })
}

export const revertTask = (id) => {
  return new Promise((resolve, reject) => {
    axios.request({
      url: baseUrl + '/revertTask',
      data: {
        id
      },
      method: 'post'
    }).then((res) => {
      resolve(res)
    }, (err) => {
      reject(err)
    })
  })
}

/**
 * 开始任务
 */
export const startTask = (id) => {
  return new Promise((resolve, reject) => {
    axios.request({
      url: baseUrl + '/startTask',
      data: {
        id
      },
      method: 'post'
    }).then((res) => {
      resolve(res)
    }, (err) => {
      reject(err)
    })
  })
}

/**
 * 停止任务
 */
export const stopTask = (id) => {
  return new Promise((resolve, reject) => {
    axios.request({
      url: baseUrl + '/stopTask',
      data: {
        id
      },
      method: 'post'
    }).then((res) => {
      resolve(res)
    }, (err) => {
      reject(err)
    })
  })
}

/**
 * 删除任务
 */
export const delTask = (id) => {
  return new Promise((resolve, reject) => {
    axios.request({
      url: baseUrl + '/delTask',
      data: {
        id
      },
      method: 'post'
    }).then((res) => {
      resolve(res)
    }, (err) => {
      reject(err)
    })
  })
}

// 公众号文章相关
export const getArticalList = ({
  pageSize,
  pageNo,
  key,
  time,
  bizId
}) => {
  return new Promise((resolve, reject) => {
    const data = {
      PageSize: pageSize || 10,
      PageNo: pageNo || 0,
      key,
      timerange: time || '',
      bizId
    }
    axios.request({
      url: baseUrl + '/articals',
      method: 'get',
      params: data
    }).then((res) => {
      resolve(res)
    }, (err) => {
      reject(err)
    })
  })
}

export const getBiz = (id) => {
  return new Promise((resolve, reject) => {
    axios.request({
      url: baseUrl + '/getBiz',
      method: 'get',
      params: {
        id
      }
    }).then(res => {
      resolve(res)
    }, err => {
      reject(err)
    })
  })
}
