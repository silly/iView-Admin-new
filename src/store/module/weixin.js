import {
  getWeixinList,
  getAccountTypes,
  addToTaskList,
  getTaskList,
  startTask,
  revertTask,
  stopTask,
  delTask,
  getArticalList,
  getBiz
} from '@/api/weixin'

export default {
  state: {
    loading: false,
    accountName: '',
    accountTypes: [],
    weixinList: [],
    weixinTaskList: [],
    weixinTaskPage: {
      type: '',
      status: '0',
      key: '', // 关键字
      current: 1, // 分页
      pageSize: 10,
      total: 0
    },
    weixinArticalList: [],
    weixinArticalPage: {
      key: '', // 搜索关键字
      time: '', // 搜索文章日期范围
      current: 1, // 分页
      pageSize: 10,
      total: 0
    },
    selectedAccountType: '',
    total: 0,
    current: 1,
    pageSize: 10,
    pubAccountdetail: {
      detail: {},
      tasks: [],
      articals: [],
      pageNo: -1,
      selectArtical: {}
    }
  },
  mutations: {
    setAccountName (state, accountName) {
      state.accountName = accountName
    },
    setAccountType (state, types) {
      state.accountTypes = types
      state.current = 1
    },
    setSelectedAccountType (state, accountType) {
      state.selectedAccountType = accountType
    },
    setWeixinList (state, {list, pageNo, total}) {
      state.weixinList = list
      state.current = pageNo
      state.total = total
    },
    changePagesize (state, pageSize) {
      state.pageSize = pageSize
    },
    changedArticalPageSize: (state, pageSize) => {
      state.weixinArticalPage.pageSize = pageSize
    },
    setArticalSearchKey: (state, searchKey) => {
      state.weixinArticalPage.key = searchKey
    },
    setArticalTimerange: (state, timerange) => {
      console.log(timerange)
      state.weixinArticalPage.time = `${timerange[0] + '|' + timerange[1]}`
    },
    setArticalList (state, {list, pageNo, total}) {
      state.weixinArticalList = list
      state.weixinArticalPage.current = pageNo
      state.weixinArticalPage.total = total
    },
    setTaskName (state, taskName) {
      state.weixinTaskPage.key = taskName
    },
    setTaskStatus (state, status) {
      state.weixinTaskPage.status = status
    },
    changeTaskListPageSize (state, pageSize) {
      state.weixinTaskPage.pageSize = pageSize
    }
  },
  actions: {
    onSearch ({dispatch}) {
      dispatch('loadList', 1)
    },
    init1 ({dispatch, state}) {
      console.log('init1')
      if (state.weixinList.length === 0) {
        dispatch('loadList', 1)
      }
    },
    setSelectedAccountType ({
      commit,
      dispatch
    }, type) {
      commit('setSelectedAccountType', type)
      dispatch('loadList', 1)
    },
    getAccountTypes ({commit}) {
      getAccountTypes().then((res) => {
        commit('setAccountType', res.data)
      })
    },
    loadList: ({
      commit,
      state,
      dispatch
    }, page) => {
      console.log('loadList')
      if (state.accountTypes.length === 0) {
        dispatch('getAccountTypes')
      }
      state.loading = true
      getWeixinList({
        pageNo: page - 1,
        pageSize: state.pageSize,
        Type: state.selectedAccountType || '',
        Name: state.accountName || ''
      }).then((res) => {
        state.loading = false
        commit('setWeixinList', {
          list: res.data,
          pageNo: page,
          total: res.count
        })
      }).catch(err => {
        console.log(err)
      })
    },
    addWeixinTask: ({
      commit,
      state,
      dispatch
    }, {Name, QrCodeLink}) => {
      return new Promise((resolve, reject) => {
        if (Name && QrCodeLink) {
          addToTaskList(Name, QrCodeLink).then(res => {
            resolve(res)
          }).catch(err => {
            console.log(err)
            reject(err)
          })
        } else {
          reject(new Error('name or url cannot be null'))
        }
      })
    },
    getTaskList: ({
      commit,
      state,
      dispatch
    }, page) => {
      state.loading = true
      getTaskList({
        key: state.weixinTaskPage.key,
        status: state.weixinTaskPage.status,
        pageNo: page - 1,
        pageSize: state.weixinTaskPage.pageSize,
        type: state.weixinTaskPage.type
      })
        .then(res => {
          state.loading = false
          state.weixinTaskPage.current = page
          state.weixinTaskList = res.data
          state.weixinTaskPage.total = res.count
        })
        .catch(err => {
          console.log(err)
        })
    },
    revertTask: ({
      commit
    }, {id}) => {
      return revertTask(id)
    },
    startTask: ({
      commit
    }, {id}) => {
      return startTask(id)
    },
    stopTask: ({
      commit
    }, {id}) => {
      return stopTask(id)
    },
    delTask: ({
      commit
    }, {id}) => {
      return delTask(id)
    },
    getArticalsList: ({
      commit,
      state
    }, page) => {
      state.loading = true
      getArticalList({
        pageNo: page - 1,
        pageSize: state.weixinArticalPage.pageSize,
        key: state.weixinArticalPage.key,
        time: state.weixinArticalPage.time
      }).then(res => {
        state.loading = false
        commit('setArticalList', {
          list: res.data,
          pageNo: page,
          total: res.count
        })
      }).catch(err => {
        console.log(err)
      })
    },
    getBiz: ({
      commit,
      state,
      dispatch
    }, {id, name}) => {
      if (!state.pubAccountdetail.detail || state.pubAccountdetail.detail.Id !== id) {
        state.pubAccountdetail = {
          detail: {},
          tasks: [],
          articals: [],
          pageNo: -1
        }
        state.loading = true
        dispatch('getTaskByName', name)
        getBiz(id).then(res => {
          state.loading = false
          state.pubAccountdetail.detail = res.data
          if (res.data.Name !== name) {
            dispatch('getTaskByName', res.data.Name)
          }
          dispatch('getArticalListByBiz', 0)
        }).catch(err => {
          console.log(err)
        })
      }
    },
    getArticalListByBiz: ({commit, state}) => {
      getArticalList({
        bizId: state.pubAccountdetail.detail.Id,
        pageNo: state.pubAccountdetail.pageNo === -1
          ? 0
          : state.pubAccountdetail.pageNo + 1
      }).then(res => {
        state.pubAccountdetail.pageNo = ++state.pubAccountdetail.pageNo
        state.pubAccountdetail.articals = [
          ...state.pubAccountdetail.articals,
          ...res.data
        ]
      }).catch(err => {
        console.log(err)
      })
    },
    selectArtical: ({
      commit,
      state
    }, artical) => {
      state.pubAccountdetail.selectArtical = artical
      console.log(artical)
    },
    getTaskByName: ({
      commit,
      state
    }, name) => {
      getTaskList({name}).then(res => {
        state.pubAccountdetail.tasks = [
          ...state.pubAccountdetail.tasks,
          ...res.data
        ]
      }).catch(err => {
        console.log(err)
      })
    }
  }
}
