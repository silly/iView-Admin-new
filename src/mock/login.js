import { getParams } from '@/libs/util'
const USER_MAP = {
  // super_admin: {
  //   name: 'super_admin',
  //   user_id: '1',
  //   access: ['super_admin', 'admin'],
  //   token: 'super_admin',
  //   avator: 'https://file.iviewui.com/dist/a0e88e83800f138b94d2414621bd9704.png'
  // },
  admin: {
    name: 'admin',
    passwd: 'passwd_admin',
    user_id: '2',
    access: ['admin'],
    token: 'admin',
    avator: 'https://avatars0.githubusercontent.com/u/20942571?s=460&v=4'
  }
}

export const login = req => {
  req = JSON.parse(req.body)
  if (USER_MAP[req.userName] && req.password === USER_MAP[req.userName].passwd) {
    return {
      code: 200,
      data: {token: USER_MAP[req.userName].token},
      msg: ''
    }
  } else {
    return {
      code: 500,
      data: null,
      msg: '用户名或密码错误'
    }
  }
}

export const getUserInfo = req => {
  const params = getParams(req.url)
  if (!USER_MAP[params.token] || !USER_MAP[params.token].token === params.token) {
    return {
      code: 500,
      data: null,
      msg: '登录状态过期'
    }
  } else {
    const data = USER_MAP[params.token]
    delete data.passwd
    return {
      code: 200,
      data,
      msg: ''
    }
  }
}

export const logout = req => {
  return {
    code: 200,
    data: null,
    msg: ''
  }
}
